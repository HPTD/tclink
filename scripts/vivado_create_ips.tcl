########################################################################
# The Xilinx Vivado EULA does not allow the distribution of IP cores
# or parts thereof. Therefore the IP core information is distributed
# in the form of Tcl scripts.
#
# This Vivado Tcl script finds IP core creation scripts matching a
# fixed file name pattern, and runs all found scripts to generate the
# corresponding IP cores.
#
# Run from the linux shell command line of your cms-tcds2-firmware area:
#   vivado -mode batch -notrace -nolog -nojou -quiet -source scripts/vivado_create_ips.tcl
# or in the Vivado Tcl console:
#   source vivado_create_ips.tcl
# In order to generate all IP cores directly for the correct target FPGA,
# add this to the command line using the '-tclargs' flag. For example:
#   vivado -mode batch -notrace -nolog -nojou -quiet -source scripts/vivado_create_ips.tcl -tclargs -target-part xcku15p-ffva1760-2-e
########################################################################

package require cmdline

variable script_dir [file dirname [file normalize [info script]]]
set env(TCLLIBPATH) [list $script_dir]
lappend ::auto_path $script_dir

########################################################################

# The IP core generation scripts are are found by name using the
# following pattern.
set script_name_pattern_base "vivado_create_ip_"

# This is how we want to call Vivado.
set vivado_cmd {vivado -mode batch -notrace -nolog -nojou -quiet -source}

set sep_line [string repeat "-" 60]

########################################################################

proc glob_recursive {{dir .} {filespec *} {types {b c f l p s}}} {
    set files [glob -nocomplain -types $types -dir $dir -- $filespec]
    foreach x [glob -nocomplain -types {d} -dir $dir -- *] {
        set files [concat $files \
                   [glob_recursive [file join [pwd] $x] $filespec $types]]
    }
    set filelist {}
    foreach x $files {
        lappend filelist [file normalize $x]
    }
    return $filelist;
}

########################################################################

set parameters {
    {target-part.arg "" "The FPGA to target"}
    {target-board.arg "" "The evaluation board to target"}
    {ip-name-pattern.arg "*" "Pattern describing which IP core(s) to generate"}
}

set usage "- A script to (re)generate Xilinx IP cores from stored parameters"

if { [catch {array set options [cmdline::getoptions ::argv $parameters $usage]}] } {
    puts [cmdline::usage $parameters $usage]
    exit 1
}

# Find all the Vivado IP core creation scripts.
set ip_name_pattern $options(ip-name-pattern)
set script_name_pattern "${script_name_pattern_base}${ip_name_pattern}.tcl"
set ip_scripts [glob_recursive . $script_name_pattern]
set s_or_not ""
set tmp [llength $ip_scripts]
if { $tmp > 1 || $tmp == 0} {
    set s_or_not "s"
}

set ip_scripts [lsort $ip_scripts]

puts "$sep_line"
puts "Found [llength $ip_scripts] matching IP core creation script$s_or_not"
puts "$sep_line"

# Now process all found scripts.
foreach {script_name} $ip_scripts {

    # Derive the Vivado project name from the script name. (NOTE: This
    # relies on a naming convention.)
    set vivado_project_name [file rootname [file tail $script_name]]

    # Derive the IP core name from the vivado project name.
    set ip_name [regsub ***=$script_name_pattern_base $vivado_project_name ""]

    puts "Processing [file tail "$ip_name"]"
    puts "$sep_line"

    # Derive the expected Vivado project directory name from the
    # project name.
    set dir_name $vivado_project_name

    # Remove any possible left-over Vivado project directory.
    file delete -force -- $dir_name

    # Have Vivado run the script and generate the IP core.
    set args ""
    set target_part $options(target-part)
    if { [string length $target_part] != 0 } {
        set args [concat $args "-target-part \"$target_part\""]
    }
    set target_board $options(target-board)
    if { [string length $target_board] != 0 } {
        set args [concat $args "-target-board \"$target_board\""]
    }
    set full_args ""
    if { [string length $args] != 0 } {
        set full_args "-tclargs $args"
    }
    set full_cmd [concat $vivado_cmd $script_name $full_args]
    set status [catch {exec {*}$full_cmd} err]
    if { $status != 0 } {
        puts "A problem occurred:"
        set tmp {}
        foreach i [split $err "\n"] {
            append tmp "  !!! $i\n"
        }
        puts -nonewline $tmp
        break
    } else {

        set target_base_name [file dirname $script_name]
        set target_sub_name $ip_name
        set target_dir_name [file join $target_base_name $target_sub_name]
        file delete -force $target_dir_name
        file mkdir $target_dir_name

        # Find the produced IP core name and move the produced IP core
        # to where it should go. (I.e. to where the original script
        # lives.)
        set ip_file_name "$ip_name.xci"
        set created_ip_file [lindex [glob_recursive $dir_name $ip_file_name] 0]
        file rename $created_ip_file $target_dir_name

        # Find the produced example design files (or at least the ones
        # under 'imports') and copy these as well.
        set ex_file_name "imports"
        set created_ex_dir [glob_recursive $dir_name $ex_file_name d]
        if { [llength $created_ex_dir] > 0 } {
            file rename $created_ex_dir [file join $target_dir_name "example_imports"]
        }
    }

    # Remove the Vivado project directory.
    file delete -force -- $dir_name
}

puts "Done"
puts "$sep_line"

########################################################################
