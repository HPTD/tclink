#######################################################
###                  SLAVE
#######################################################
#-----------------------------------------------------
### RXSLIDE MODE
# OBS: This design can only achieve a slave fixed-latency in PMA mode. In order to achieve a fixed-latency in PCS mode, the roulette approach has to be implemented
#-----------------------------------------------------
set_property RXSLIDE_MODE PMA [get_cells -hierarchical -regexp -filter {REF_NAME =~ {.*GT(H|Y)E(3|4)_CHANNEL.*} && NAME =~ {.*slave_timing.*}}]
#-----------------------------------------------------
