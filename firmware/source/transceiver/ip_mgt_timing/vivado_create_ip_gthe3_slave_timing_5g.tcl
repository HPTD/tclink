########################################################################

# These describe the IP core to be built.
set ip_name gtwizard_ultrascale
set ip_vendor xilinx.com
set ip_library ip
set ip_version 1.7

# This is the name to give to the generated module.
set module_name gthe3_slave_timing_5g

# These are the settings to apply to the generated module.
set module_properties {
    CONFIG.GT_TYPE {GTH}
    CONFIG.CHANNEL_ENABLE {X0Y4}
    CONFIG.TX_MASTER_CHANNEL {X0Y4}
    CONFIG.RX_MASTER_CHANNEL {X0Y4}
    CONFIG.TX_LINE_RATE {5.130112}
    CONFIG.TX_PLL_TYPE {QPLL0}
    CONFIG.TX_REFCLK_FREQUENCY {320.632}
    CONFIG.TX_USER_DATA_WIDTH {16}
    CONFIG.TX_INT_DATA_WIDTH {16}
    CONFIG.TX_OUTCLK_SOURCE {TXPLLREFCLK_DIV1}
    CONFIG.RX_LINE_RATE {5.130112}
    CONFIG.RX_PLL_TYPE {QPLL1}
    CONFIG.RX_REFCLK_FREQUENCY {320.632}
    CONFIG.RX_USER_DATA_WIDTH {16}
    CONFIG.RX_INT_DATA_WIDTH {16}
    CONFIG.RX_BUFFER_MODE {0}
    CONFIG.RX_EQ_MODE {DFE}
    CONFIG.RX_JTOL_FC {3.0774517}
    CONFIG.RX_COMMA_ALIGN_WORD {2}
    CONFIG.RX_COMMA_SHOW_REALIGN_ENABLE {false}
    CONFIG.RX_SLIDE_MODE {PCS}
    CONFIG.ENABLE_OPTIONAL_PORTS {dmonitorclk_in drpaddr_in drpclk_in drpdi_in drpen_in drpwe_in loopback_in rxdfeagcovrden_in rxdfelfovrden_in rxdfelpmreset_in rxdfeutovrden_in rxdfevpovrden_in rxlpmen_in rxlpmgcovrden_in rxlpmhfovrden_in rxlpmlfklovrden_in rxlpmosovrden_in rxosovrden_in rxpolarity_in rxprbscntreset_in rxprbssel_in txpippmen_in txpippmovrden_in txpippmpd_in txpippmsel_in txpippmstepsize_in txpolarity_in txprbsforceerr_in txprbssel_in dmonitorout_out drpdo_out drprdy_out qpll0lock_out qpll1lock_out rxprbserr_out rxprbslocked_out txbufstatus_out}
    CONFIG.RX_REFCLK_SOURCE {X0Y4 clk0+2}
    CONFIG.TX_REFCLK_SOURCE {X0Y4 clk1+2}
    CONFIG.RX_RECCLK_OUTPUT {}
    CONFIG.LOCATE_TX_USER_CLOCKING {EXAMPLE_DESIGN}
    CONFIG.LOCATE_RX_USER_CLOCKING {EXAMPLE_DESIGN}
    CONFIG.TXPROGDIV_FREQ_SOURCE {QPLL0}
    CONFIG.TXPROGDIV_FREQ_VAL {320.632}
    CONFIG.FREERUN_FREQUENCY {125}
}

# Do we need the example design as well?
set include_example_design false

# The default part and evaluation board to target (unless overridden
# from the command line).
set default_part "xcku040-ffva1156-2-e"
set default_board ""

########################################################################

package require vivado_utils

vivado_utils::run_vivado_create_ip \
    $ip_name $ip_vendor $ip_library $ip_version \
    $module_name $module_properties \
    $include_example_design \
    $default_part $default_board \
    $::argv

########################################################################
