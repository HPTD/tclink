########################################################################

# These describe the IP core to be built.
set ip_name clk_wiz
set ip_vendor xilinx.com
set ip_library ip
set ip_version 6.0

# This is the name to give to the generated module.
set module_name mmcme3_slave

# These are the settings to apply to the generated module.
set module_properties {
    CONFIG.USE_PHASE_ALIGNMENT {true}
    CONFIG.USE_DYN_PHASE_SHIFT {true}
    CONFIG.PRIM_IN_FREQ {320.632}
    CONFIG.PHASESHIFT_MODE {LATENCY}
    CONFIG.SECONDARY_SOURCE {Single_ended_clock_capable_pin}
    CONFIG.CLKIN1_JITTER_PS {31.18}
    CONFIG.CLK_OUT1_USE_FINE_PS_GUI {true}
    CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {40.079}
    CONFIG.PRIM_SOURCE {No_buffer}
    CONFIG.CLKOUT1_DRIVES {Buffer}
    CONFIG.CLKOUT2_DRIVES {Buffer}
    CONFIG.CLKOUT3_DRIVES {Buffer}
    CONFIG.CLKOUT4_DRIVES {Buffer}
    CONFIG.CLKOUT5_DRIVES {Buffer}
    CONFIG.CLKOUT6_DRIVES {Buffer}
    CONFIG.CLKOUT7_DRIVES {Buffer}
    CONFIG.MMCM_DIVCLK_DIVIDE {8}
    CONFIG.MMCM_CLKFBOUT_MULT_F {25.000}
    CONFIG.MMCM_CLKIN1_PERIOD {3.119}
    CONFIG.MMCM_CLKIN2_PERIOD {10.0}
    CONFIG.MMCM_CLKOUT0_DIVIDE_F {25.000}
    CONFIG.MMCM_CLKOUT0_USE_FINE_PS {true}
    CONFIG.CLKOUT1_JITTER {120.014}
    CONFIG.CLKOUT1_PHASE_ERROR {79.806}
}

# Do we need the example design as well?
set include_example_design false

# The default part and evaluation board to target (unless overridden
# from the command line).
set default_part "xcku040-ffva1156-2-e"
set default_board "xilinx.com:kcu105:part0:1.6"

########################################################################

package require vivado_utils

vivado_utils::run_vivado_create_ip \
    $ip_name $ip_vendor $ip_library $ip_version \
    $module_name $module_properties \
    $include_example_design \
    $default_part $default_board \
    $::argv

########################################################################
